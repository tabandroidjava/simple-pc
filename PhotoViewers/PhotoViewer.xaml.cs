﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;


namespace SimplePC.PhotoViewers
{
	public partial class PhotoViewer : Window
	{
		public static readonly IReadOnlyList<string> EXTENSIONS = new List<string>
		{
			".ANI", ".CLP", ".CMP", ".CMW", ".CUR", ".DIC", ".EMF", ".EPS", ".EXIF", ".FLC", ".GIF", ".HDP",".ICO",
			".IFF", ".JPG", ".JP2", ".PBM", ".PCX", ".PNG", ".PSD", "RAS", ".SGI", ".TGA", ".TIFF", ".WPG"
		};

		public PhotoViewer(string photoPath, Window owner)
		{
			InitializeComponent();
			Owner = owner;
			title.Text = $"HÌNH ẢNH: {Path.GetFileNameWithoutExtension(photoPath)}";
			image.Source = new BitmapImage(new Uri(photoPath));
			foreach (string f in Directory.GetFiles(Path.GetDirectoryName(photoPath)))
				if (EXTENSIONS.Contains(Path.GetExtension(f).ToUpper())) uris.Add(new Uri(f));

			uris.Sort((a, b) => a.LocalPath.CompareTo(b.LocalPath));
			for (; index < uris.Count && uris[index].LocalPath != photoPath; ++index) ;
		}


		public new async void Show()
		{
			base.Show();
			await Task.Yield();
			Focus();
		}


		private void OnClick_Close(object sender, RoutedEventArgs e) => Close();


		private readonly List<Uri> uris = new List<Uri>();
		private int index;
		private void Window_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Left:
				case Key.Up:
					index = --index < 0 ? 0 : index;
					break;

				case Key.Right:
				case Key.Down:
					index = ++index < uris.Count ? index : uris.Count - 1;
					break;

				default: return;
			}

			image.Source = new BitmapImage(uris[index]);
			title.Text = $"HÌNH ẢNH: {Path.GetFileNameWithoutExtension(uris[index].LocalPath)}";
		}
	}
}
