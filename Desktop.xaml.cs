﻿using Gma.System.MouseKeyHook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Unosquare.FFME;


namespace SimplePC
{
	public partial class Desktop : Window
	{
		private static Desktop instance;
		public Desktop()
		{
			App.Current.MainWindow = instance = this;
			InitializeComponent();
			InitVideoWallpaper();

			#region Khởi tạo {icons}
			iconYoutube.GetChild<Image>().Source = new BitmapImage(new Uri("pack://application:,,,/Images/youtube.png"));
			iconTV.GetChild<Image>().Source = new BitmapImage(new Uri("pack://application:,,,/Images/tv.png"));
			iconVideo.GetChild<Image>().Source = new BitmapImage(new Uri("pack://application:,,,/Images/video.png"));
			iconHinhAnh.GetChild<Image>().Source = new BitmapImage(new Uri("pack://application:,,,/Images/hinhanh.png"));
			iconWindows.GetChild<Image>().Source = new BitmapImage(new Uri("pack://application:,,,/Images/windows.ico"));
			iconGame.GetChild<Image>().Source = new BitmapImage(new Uri("pack://application:,,,/Images/game.png"));
			#endregion

			#region Đảm bảo luôn maximize, foreground cho đến khi người dùng tương tác với Desktop
			bool interact = false;
			var h = Hook.GlobalEvents();
			h.MouseDown += Ev;
			h.KeyDown += Ev;
			void Ev(object _, object __) => interact = true;
			TryForeground();


			async void TryForeground()
			{
				var p = Process.GetCurrentProcess();
				do
				{
					if (!IsActive) p.MaximizeMainWindow();
					await Task.Delay(1000);
				} while (!interact);

				h.MouseDown -= Ev;
				h.KeyDown -= Ev;
				h.Dispose();
			}
			#endregion

			#region X Button Popup
			hideXTimer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Background, (_, __) =>
			{
				if (++hideX_time < HIDE_X_TIMEOUT) return;
				xButton.IsOpen = false;
				hideXTimer.Stop();
			}, App.Current.Dispatcher);
			#endregion


			async void InitVideoWallpaper()
			{
				videoWallpaper.Volume = 1 / 3d;
				videoWallpaper.MediaEnded += async (_, __) =>
					  {
						  await videoWallpaper.Close();
						  await videoWallpaper.Open(new Uri(App.config.videoWallpaperPath));
						  if (IsActive) PlayVideoWallpaper();
					  };

				await videoWallpaper.Open(new Uri(App.config.videoWallpaperPath));
				if (IsActive) PlayVideoWallpaper();
			}
		}


		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel =
#if DEBUG
				false
#else
				true
#endif
				;
		}


		private void Window_Closed(object _, EventArgs __)
		{
			enableXButton = false;
			clickX = null;
			xHook.Dispose();
			videoWallpaper.Close();
		}


		#region Icons
		private const int DELAY_TO_MAXIMIZE = 4000;

		private readonly ProcessStartInfo youtubeInfo = new ProcessStartInfo
		{
			FileName = App.config.youtube.fileName,
			Arguments = App.config.youtube.arguments
		};
		private async void OnClick_Youtube(object sender, RoutedEventArgs e)
		{
			iconYoutube.IsEnabled = false;
			Process.Start(youtubeInfo);
			await Task.Delay(DELAY_TO_MAXIMIZE);

			foreach (var p in Process.GetProcessesByName("msedge")) p.MaximizeMainWindow();
			foreach (var p in Process.GetProcessesByName("chrome")) p.MaximizeMainWindow();
			iconYoutube.IsEnabled = true;
			clickX += CloseWebBrowsers;
			enableXButton = true;
		}


		private readonly ProcessStartInfo tvInfo = new ProcessStartInfo
		{
			FileName = App.config.TV.fileName,
			Arguments = App.config.TV.arguments
		};
		private async void OnClick_TV(object sender, RoutedEventArgs e)
		{
			iconTV.IsEnabled = false;
			foreach (var p in Process.GetProcessesByName("msedge")) p.CloseMainWindow();
			foreach (var p in Process.GetProcessesByName("chrome")) p.CloseMainWindow();
			Process.Start(tvInfo);
			await Task.Delay(DELAY_TO_MAXIMIZE);

			foreach (var p in Process.GetProcessesByName("msedge")) p.MaximizeMainWindow();
			foreach (var p in Process.GetProcessesByName("chrome")) p.MaximizeMainWindow();
			iconTV.IsEnabled = true;
			clickX += CloseWebBrowsers;
			enableXButton = true;
		}


		private void OnClick_Video(object sender, RoutedEventArgs e) => OnClickFolder(iconVideo, App.config.videoPath);


		private void OnClick_HinhAnh(object sender, RoutedEventArgs e) => OnClickFolder(iconHinhAnh, App.config.photoPath);


		private async void OnClickFolder(Button button, string path)
		{
			button.IsEnabled = false;
			string name = Path.GetFileName(path);
			bool found = false;
			foreach (var p in Process.GetProcessesByName("explorer"))
				if (p.MainWindowTitle == name)
					if (found) p.CloseMainWindow();
					else
					{
						found = true;
						p.MaximizeMainWindow();
					}

			if (!found)
			{
				Process.Start("explorer", path);
				await Task.Delay(DELAY_TO_MAXIMIZE);
				foreach (var p in Process.GetProcessesByName("explorer"))
					if (p.MainWindowTitle == name)
					{
						p.MaximizeMainWindow();
						break;
					}
			}

			button.IsEnabled = true;
		}


		private async void OnClick_Windows(object sender, RoutedEventArgs e)
		{
			iconWindows.IsEnabled = false;
			if (!Util.isTaskBarVisible) Util.isTaskBarVisible = true;
			var k = App.bannedKeys;
			k.Remove(KeyboardListener.Key.LControlKey);
			k.Remove(KeyboardListener.Key.RControlKey);
			System.Windows.Forms.SendKeys.SendWait("^{ESC}");
			await Task.Delay(500);
			k.Add(KeyboardListener.Key.LControlKey);
			k.Add(KeyboardListener.Key.RControlKey);
			iconWindows.IsEnabled = true;
		}


		private void OnClick_Game(object sender, RoutedEventArgs e) => new Games.Game().Show();


		private static void CloseWebBrowsers()
		{
			foreach (var p in Process.GetProcessesByName("msedge")) p.CloseMainWindow();
			foreach (var p in Process.GetProcessesByName("chrome")) p.CloseMainWindow();
			clickX -= CloseWebBrowsers;
			enableXButton = false;
		}
		#endregion


		private CancellationTokenSource cancelMaximize;
		private async void Window_Activated(object sender, EventArgs e)
		{
#if !DEBUG
			if (Util.isTaskBarVisible) Util.isTaskBarVisible = false;
			var token = (cancelMaximize = new CancellationTokenSource()).Token;
			await Task.Run(async () =>
			{
				await Task.Delay(DELAY_TO_MAXIMIZE);
				if (token.IsCancellationRequested) return;

				foreach (var p in Process.GetProcessesByName("msedge")) p.MaximizeMainWindow();
				foreach (var p in Process.GetProcessesByName("chrome")) p.MaximizeMainWindow();
				foreach (var p in Process.GetProcessesByName("mpc-be64")) p.MaximizeMainWindow();
				string videoName = Path.GetFileName(App.config.videoPath);
				string photoName = Path.GetFileName(App.config.photoPath);
				foreach (var p in Process.GetProcessesByName("explorer"))
					if (p.MainWindowTitle == videoName || p.MainWindowTitle == photoName) p.MaximizeMainWindow();
			});
#endif
			if (videoWallpaper.IsOpen) PlayVideoWallpaper();
		}


		private void Window_Deactivated(object sender, EventArgs e)
		{
#if !DEBUG
			cancelMaximize.Cancel();
			cancelMaximize.Dispose();
#endif
			if (videoWallpaper.IsOpen) videoWallpaper.Pause();
		}


		#region X Button Popup
		public static event Action clickX;
		public void OnClick_X(object _, RoutedEventArgs __) => clickX();


		private const int HIDE_X_TIMEOUT = 5;
		private int hideX_time;
		private readonly DispatcherTimer hideXTimer;
		private readonly IKeyboardMouseEvents xHook = Hook.GlobalEvents();
		private static bool __enableXButton;
		/// <summary>
		/// Nếu <see langword="true"/> thì sẽ tự động hiện X Button khi Key down/ mouse move/ mouse down và tự động ẩn sau khoảng thời gian không có key/mouse <para/>
		/// Nếu <see langword="false"/> thì sẽ tắt hoàn toàn X Button
		/// </summary>
		public static bool enableXButton
		{
			get => __enableXButton;

			set
			{
				if (__enableXButton == value) return;
				instance.xButton.IsOpen = instance.hideXTimer.IsEnabled = __enableXButton = value;
				instance.hideX_time = 0;
				if (value)
				{
					instance.xHook.KeyDown += OnMouseKeyEvents;
					instance.xHook.MouseMove += OnMouseKeyEvents;
					instance.xHook.MouseDown += OnMouseKeyEvents;
				}
				else
				{
					instance.xHook.KeyDown -= OnMouseKeyEvents;
					instance.xHook.MouseMove -= OnMouseKeyEvents;
					instance.xHook.MouseDown -= OnMouseKeyEvents;
				}


				void OnMouseKeyEvents(object _, object __)
				{
					instance.hideX_time = 0;
					if (!instance.xButton.IsOpen) instance.xButton.IsOpen = true;
					if (!instance.hideXTimer.IsEnabled) instance.hideXTimer.IsEnabled = true;
				}
			}
		}
		#endregion


		#region Video Wallpaper
		private void Window_KeyDown(object sender, KeyEventArgs e)
		{
			if (!videoWallpaper.IsOpen) return;
			switch (e.Key)
			{
				case Key.Space:
				case Key.Enter:
				case Key.MediaPlayPause:
					if (videoWallpaper.IsPaused) PlayVideoWallpaper(); else videoWallpaper.Pause();
					break;

				case Key.Right:
					if (!videoWallpaper.IsSeeking) videoWallpaper.Seek(videoWallpaper.Position + TimeSpan.FromSeconds(5));
					break;

				case Key.Left:
					if (!videoWallpaper.IsSeeking) videoWallpaper.Seek(videoWallpaper.Position - TimeSpan.FromSeconds(5));
					break;
			}
		}


		private void Window_MousePlayPause(object sender, MouseButtonEventArgs e)
		{
			if (!videoWallpaper.IsOpen) return;
			if (videoWallpaper.IsPaused) PlayVideoWallpaper(); else videoWallpaper.Pause();
		}


		private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			if (!videoWallpaper.IsOpen) return;
			if (!videoWallpaper.IsSeeking)
				videoWallpaper.Seek(e.Delta > 0 ? videoWallpaper.Position + TimeSpan.FromSeconds(5)
									: videoWallpaper.Position - TimeSpan.FromSeconds(5));
		}


		private async void PlayVideoWallpaper()
		{
			await videoWallpaper.Play();
			if (!IsActive) videoWallpaper.Pause();
		}
		#endregion
	}
}