﻿Giúp người già, người không giỏi máy tính có thể sử dụng dễ dàng nhất.
Hướng dẫn sử dụng: portable không cần cài, chỉnh sửa file Config.json trong thư mục của SimplePC để cài đặt App.
Vào Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\UserInit thêm đường dẫn exe đến SimplePC này ở đầu tiên để đảm bảo App sẽ chạy đầu tiên.

Tính năng:
- Ẩn hầu hết môi trường Windows, vai trò như 1 "màn hình Desktop"
- Hình nền có thể thay đổi và tự động chuyển hình (cài thư mục hình nền và thời gian delay trong Config.json), nếu file trong thư mục bị thay đổi thì cập nhật hình nền => Khi SimplePC bị mất focus/deactivate thì hình nền tạm ngưng.
- Hình nền có 2 chế độ: tuần tự slideshow hoặc random slideshow (cài trong Config.json). Cả 2 chế độ đều lưu index của hình vào file khi tắt máy.
- Có 4 icon cố định (tương lai sẽ cho phép tùy ý thêm icon), bao gồm: Youtube, TV, Phim, Hình Ảnh. Cài đặt (path, arg) của icon trong Config.json
- Khi click vô 1 icon thì sẽ đảm bảo chỉ có duy nhất 1 cửa sổ được mở lên và luôn được Maximize, Foreground
- SimplePC luôn luôn kiểm tra các cửa sổ của các icon và Maximize tất cả cửa sổ, bao gồm các app được mở lên như MPC-BE, PhotoViewer (delay 0.5 giây) => Khi SimplePC bị mất focus/deactivate thì sẽ tạm ngưng việc tìm kiếm và Maximize cửa sổ.
- Nếu file Config.json được thêm/xóa/chỉnh sửa/đổi tên thì SimplePC sẽ Restart.
- Lọc bàn phím: Chỉ cho phép những phím được chỉ định (chỉ định phím trong Config.json : allowedKeys). Xem các phím trong file ALL_KEYS.txt. Những phím khác sẽ bị chặn.


==> Mục tiêu: biến hệ điều hành Windows trở thành "Đơn nhiệm"(Single-task), các cửa sổ luôn Maximize và chỉ có thể mở/đóng cửa sổ.