﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;


namespace SimplePC.MediaPlayers
{
	public partial class MediaPlayer : Window
	{
		public static IReadOnlyList<string> EXTENSIONS = new List<string>
		{
			".3G2", ".3GP", ".4XM", ".A64", ".AA", ".AAC", ".AC3", ".ACM", ".AMR", ".AV1", ".AVI", ".AVR",
			".BMV", ".FLAC", ".FLV", ".M4V", ".MKV", ".MLV", ".MP2", ".MP3", ".MP4", ".WAV", ".WEBM", ".M4A"
		};
		private static readonly IReadOnlyList<string> AUDIO_EXTENSIONS = new List<string>
		{
			".AAC", ".AMR", ".MP3", ".WAV", ".M4A", ".FLAC"
		};

		private readonly bool isVideoFile;
		public MediaPlayer(string mediaPath, Window owner)
		{
			InitializeComponent();
			isVideoFile = !AUDIO_EXTENSIONS.Contains(Path.GetExtension(mediaPath).ToUpper());
			Owner = owner;
			seekThumb.Visibility = frameDock.Visibility = Visibility.Hidden;
			OpenMedia(mediaPath);


			async void OpenMedia(string mediaPath)
			{
				IsEnabled = false;
				await player.Open(new Uri(mediaPath));
				IsEnabled = true;
				Focus();
				hideUI_timer = new DispatcherTimer(new TimeSpan(0, 0, 0, 1), DispatcherPriority.Background, (s, e) =>
				{
					if (++hideUI_time == HIDE_UI_TIMEOUT)
					{
						UI.Visibility = Visibility.Hidden;
						hideUI_timer.Stop();
						seekBarTimer.Stop();
					}
				}, App.Current.Dispatcher);

				#region Khởi tạo Seek Bar
				fileName.Text = Path.GetFileName(mediaPath);
				frameImage.Source = null;
				var total = player.NaturalDuration.Value;
				seekBar.Maximum = total.TotalSeconds;
				string totalTime = $"{total.Hours:00}:{total.Minutes:00}:{total.Seconds:00}";
				seekBarTimer.Tick += (s, e) =>
				{
					var elapse = player.Position;
					seekBar.Value = elapse.TotalSeconds;
					displayTime.Text = $"{elapse.Hours:00}:{elapse.Minutes:00}:{elapse.Seconds:00}/ {totalTime}";
				};
				if (isVideoFile) FrameImageTimer();
				#endregion

				player.MediaEnded += (s, e) =>
				  {
					  switch (App.config.mediaStopOption)
					  {
						  case App.Config.MediaStopOption.DoNothing: break;
						  case App.Config.MediaStopOption.Close: Close(); break;
						  case App.Config.MediaStopOption.PlayNext:
							  {
								  Close();
								  var paths = new List<string>();
								  foreach (string path in Directory.GetFiles(Path.GetDirectoryName(mediaPath)))
									  if (EXTENSIONS.Contains(Path.GetExtension(path).ToUpper()))
										  paths.Add(path);

								  paths.Sort();
								  for (int i = 0; i < paths.Count; ++i)
									  if (paths[i] == mediaPath && i < paths.Count - 1)
									  {
										  new MediaPlayer(paths[i + 1], owner).Show();
										  return;
									  }
							  }
							  break;
					  }
				  };
			}
		}


		public new async void Show()
		{
			base.Show();
			await Task.Yield();
			Focus();
		}


		#region Window Events
		private bool ΔisClosed;
		/// <summary>
		/// Luôn luôn đóng cửa sổ bằng hàm này.<br/>
		/// Không nên thu dọn bằng <see cref="Window_Closing"/> bởi vì không biết khi nào callback mới được gọi.
		/// </summary>
		public new void Close()
		{
			ΔisClosed = true;
			player.Close();
			cancelFrameTask.Cancel();
			cancelFrameTask.Dispose();

			base.Close();
		}


		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!ΔisClosed) throw new InvalidOperationException("Chỉ được đóng cửa sổ bằng cách gọi <MediaPlayer>.Close() !");
		}


		private void Window_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			OnUserInteract();
		}


		private void Window_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			OnUserInteract();
			if (player.IsPaused) player.Play();
			else player.Pause();
		}


		private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			OnUserInteract();
			switch (e.Key)
			{
				case Key.Space:
				case Key.Enter:
				case Key.MediaPlayPause:
					if (player.IsPaused) player.Play(); else player.Pause();
					break;

				case Key.Right:
					if (isVideoFile && !player.IsSeeking) player.Seek(player.Position + TimeSpan.FromSeconds(5));
					break;

				case Key.Left:
					if (isVideoFile && !player.IsSeeking) player.Seek(player.Position - TimeSpan.FromSeconds(5));
					break;

				case Key.Escape: Close(); break;
			}
		}
		#endregion


		private void closeButton_Click(object sender, RoutedEventArgs e) => Close();


		#region Auto hide UI
		private const int HIDE_UI_TIMEOUT = 5;
		private int hideUI_time;
		private DispatcherTimer hideUI_timer;

		/// <summary>
		/// Khi người dùng có tương tác chuột/ bàn phím thì bật UI và tự động ẩn nếu không còn tương tác.
		/// </summary>
		private void OnUserInteract()
		{
			if (UI.Visibility == Visibility.Hidden) UI.Visibility = Visibility.Visible;
			hideUI_time = 0;
			if (!hideUI_timer.IsEnabled) hideUI_timer.IsEnabled = true;
			if (!seekBarTimer.IsEnabled) seekBarTimer.IsEnabled = true;
		}
		#endregion


		#region Seek Bar
		private readonly DispatcherTimer seekBarTimer = new DispatcherTimer(DispatcherPriority.Normal, App.Current.Dispatcher)
		{
			Interval = new TimeSpan(0, 0, 0, 0, 100)
		};

		private void seekBar_MouseDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
			if (player.IsSeeking) return;
			double seconds = (e.GetPosition(seekBar).X / seekBar.ActualWidth) * player.NaturalDuration.Value.TotalSeconds;
			player.Seek(TimeSpan.FromSeconds(seconds));
		}


		private void seekBar_MouseEnter(object sender, MouseEventArgs e)
		{
			e.Handled = true;
			frameDock.Visibility = seekThumb.Visibility = Visibility.Visible;
			UpdateFrameDock(e.GetPosition(seekBar).X);
		}


		private void seekBar_MouseLeave(object sender, MouseEventArgs e)
		{
			e.Handled = true;
			frameDock.Visibility = seekThumb.Visibility = Visibility.Hidden;
		}


		private void seekBar_MouseMove(object sender, MouseEventArgs e)
		{
			e.Handled = true;
			double x = e.GetPosition(seekBar).X;
			UpdateFrameDock(x);
			seekThumb.SetValue(Canvas.LeftProperty, x);
			x -= frameDock.ActualWidth / 2f;
			double maxX = seekBar.ActualWidth - frameDock.ActualWidth;
			frameDock.SetValue(Canvas.LeftProperty, x < 0 ? 0 : x > maxX ? maxX : x);
		}


		private void UpdateFrameDock(double mouseX)
		{
			var pos = TimeSpan.FromSeconds((mouseX / seekBar.ActualWidth) * player.NaturalDuration.Value.TotalSeconds);
			framePosition.Text = $"{pos.Hours:00}:{pos.Minutes:00}:{pos.Seconds:00}";
			lock (positionLock) { frameTimerPosition = pos; }
		}


		private readonly CancellationTokenSource cancelFrameTask = new CancellationTokenSource();
		private TimeSpan? frameTimerPosition;
		private readonly object positionLock = new object();
		private static readonly string cacheFrame = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\SimplePC\\MediaPlayer\\cacheFrame.jpg";

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private async void FrameImageTimer()
		{
			var token = cancelFrameTask.Token;
			frameDock.Visibility = Visibility.Visible;
			var (x, y) = frameDock.RenderSize.ToPixel();
			frameDock.Visibility = Visibility.Hidden;
			string args = $" -i \"{player.Source.LocalPath}\" -vframes 1 -q: 20 -s {x}x{y} -v quiet \"{cacheFrame}\"";
			var info = new ProcessStartInfo
			{
				FileName = $"{App.path}MediaPlayers\\ffmpeg-4.3.1-2020-09-21-x64\\ffmpeg.exe",
				CreateNoWindow = true,
				UseShellExecute = false,
				RedirectStandardError = true
			};
			Process ffmpeg = null;
			token.Register(() => { if (ffmpeg?.HasExited == false) ffmpeg.Kill(); });
			lock (App.globalLock)
			{
				if (!Directory.Exists(Path.GetDirectoryName(cacheFrame)))
					Directory.CreateDirectory(Path.GetDirectoryName(cacheFrame));
				if (File.Exists(cacheFrame)) File.Delete(cacheFrame);
			}
			await Task.Run(FrameImageTimer);


			async Task FrameImageTimer()
			{
				while (true)
				{
					await Task.Delay(50);
					if (token.IsCancellationRequested) break;

					#region Giải mã tạo cacheFrame bằng ffmpeg
					lock (positionLock)
					{
						if (frameTimerPosition == null) continue;
						info.Arguments = $"-ss {frameTimerPosition.Value.TotalSeconds}{args}";
						frameTimerPosition = null;
					}

					ffmpeg = new Process { StartInfo = info };
					ffmpeg.ErrorDataReceived += (s, e) =>
					{
						if (!string.IsNullOrEmpty(e.Data))
							throw new Exception($"FFmpeg bị lỗi: {e.Data}");
					};
					ffmpeg.EnableRaisingEvents = true;
					ffmpeg.Start();
					ffmpeg.BeginErrorReadLine();
					if (!ffmpeg.WaitForExit(5000)) throw new Exception("FFmpeg bị treo !");
					#endregion

					#region Sao chép cacheFrame, tạo bitmap xong rồi xóa cacheFrame
					BitmapImage bmp = null;
					lock (App.globalLock)
					{
						if (token.IsCancellationRequested) break;
						if (File.Exists(cacheFrame))
						{
							using (var fs = new FileStream(cacheFrame, FileMode.Open))
							{
								bmp = new BitmapImage();
								bmp.BeginInit();
								bmp.StreamSource = fs;
								bmp.CacheOption = BitmapCacheOption.OnLoad;
								bmp.EndInit();
								bmp.Freeze();
							}
							File.Delete(cacheFrame);
						}
					}
					#endregion

					App.Current.Dispatcher.Invoke(() =>
					{
						// Đặt try-catch bởi vì Dispatcher có thể vẫn gọi sau khi Window đã close hoặc đã bị hủy do lỗi
						if (!token.IsCancellationRequested) try { frameImage.Source = bmp; } catch { }
					});
				}
			}
		}
		#endregion
	}
}
