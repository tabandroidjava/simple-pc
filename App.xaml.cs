﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Media;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;


namespace SimplePC
{
	public partial class App : Application
	{
		public static Config config { get; private set; }
		private readonly FileSystemWatcher watcher = new FileSystemWatcher
		{
			Path = path,
			EnableRaisingEvents = true
		};
#if !DEBUG
		private readonly KeyboardListener keyboardListener = new KeyboardListener();
#endif
		private Util.AppBarState appBarState;
		public static readonly List<KeyboardListener.Key> bannedKeys = new List<KeyboardListener.Key>()
		{
#if !DEBUG
			KeyboardListener.Key.LMenu, KeyboardListener.Key.RMenu, KeyboardListener.Key.F4,
#endif
			KeyboardListener.Key.F1,KeyboardListener.Key.F2,KeyboardListener.Key.F3,
			KeyboardListener.Key.F5,KeyboardListener.Key.F6,KeyboardListener.Key.F7,KeyboardListener.Key.F8,
			KeyboardListener.Key.F9,KeyboardListener.Key.F10,KeyboardListener.Key.F11,KeyboardListener.Key.F12,
			KeyboardListener.Key.SelectMedia,KeyboardListener.Key.LaunchMail,KeyboardListener.Key.VolumeMute,
			KeyboardListener.Key.BrowserSearch,KeyboardListener.Key.BrowserHome,
			KeyboardListener.Key.LControlKey, KeyboardListener.Key.RControlKey, KeyboardListener.Key.LWin, KeyboardListener.Key.RWin
		};

		private async void Application_Startup(object sender, StartupEventArgs e)
		{
			try
			{
				config = JsonConvert.DeserializeObject<Config>(File.ReadAllText($"{path}\\{Config.FileName}").GetPureJson());
			}
			catch (FileNotFoundException)
			{
				throw new FileNotFoundException($"Hãy tạo file {Config.FileName} ở {path} !");
			}

			#region Nếu file Config.json được thêm/xóa/thay đổi thì restart app
			watcher.Changed += (s, e) => { if (e.Name == Config.FileName) Restart(); };
			watcher.Created += (s, e) => { if (e.Name == Config.FileName) Restart(); };
			watcher.Deleted += (s, e) => { if (e.Name == Config.FileName) Restart(); };
			watcher.Renamed += (s, e) => { if (e.Name == Config.FileName) Restart(); };
			watcher.Error += (s, e) => Restart();
			#endregion

			#region Chỉ cho phép những phím được chỉ định
#if !DEBUG
			var soundPlayer = new SoundPlayer($"{path}/Sound/error.wav");
			soundPlayer.LoadAsync();
			var task = Task.CompletedTask;
			keyboardListener.allowKeys += key =>
			{
				if (!bannedKeys.Contains(key)) return true;
				if (task.IsCompleted) task = Task.Run(soundPlayer.Play);
				return false;
			};
#endif
			#endregion

			#region Ẩn taskbar
#if !DEBUG
			if ((appBarState = Util.taskBarState) == Util.AppBarState.AlwaysOnTop)
			{
				Util.taskBarState = Util.AppBarState.AutoHide;
				await Task.Delay(300);
			}
			Util.isTaskBarVisible = false;
#endif
			#endregion

			if (!Environment.Is64BitOperatingSystem) throw new NotSupportedException("Lib ffmpeg-4.3.1-2020-09-21-x64 yêu cầu hệ điều hành 64bit !");
			Unosquare.FFME.Library.FFmpegDirectory = $"{path}\\MediaPlayers\\ffmpeg-4.3.1-2020-09-21-x64";
			new Desktop().Show();
		}


		private void Application_Exit(object sender, ExitEventArgs e)
		{
			watcher.Dispose();
#if !DEBUG
			keyboardListener.Dispose();
			Util.taskBarState = appBarState;
			Util.isTaskBarVisible = true;
#endif
		}


		public static readonly string cachePath = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\SimplePC\\";
		private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			Application_Exit(null, null);
			File.WriteAllText($"{cachePath}\\ERROR.txt", $"{DateTime.Now}:\n{e.Exception}");
			MessageBox.Show($"{e.Exception}\n.See log file at {cachePath}\\ERROR.txt");
			e.Handled = true;
			Environment.Exit(1);
		}


		public sealed class Config
		{
			public static readonly string FileName = "Config.json";

			public struct WebIcon
			{
				public string fileName, arguments;
			}
			public WebIcon youtube, TV, facebook;
			public string videoPath, photoPath;

			public enum MediaStopOption
			{
				DoNothing, Close, PlayNext
			}
			public MediaStopOption mediaStopOption;
			public string videoWallpaperPath;
		}


		public static event Action onRestart;
		public static void Restart() =>
			Current.Dispatcher.Invoke(() =>
			{
				onRestart?.Invoke();
				Current.Exit += (s, e) => Process.Start(Assembly.GetEntryAssembly().Location);
				Current.Shutdown();
			});


		public static readonly string path = AppDomain.CurrentDomain.BaseDirectory;

		/// <summary>
		/// Nên sử dụng lock này nếu các worker-thread truy cập các resource phạm vi global toàn App hoặc phạm vi toàn hệ điều hành.<para/>
		/// Sau khi lock thì kiểm tra thủ công xem worker-thread có bị hủy hay có quyền truy cập ?
		/// </summary>
		public static readonly object globalLock = new object();
	}
}
