﻿using Microsoft.WindowsAPICodePack.Shell;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;


namespace SimplePC.FileExplorers
{
	public partial class FileExplorer : Window
	{
		private readonly FileSystemWatcher watcher;
		private FileExplorer(string rootFolderPath)
		{
			instance = instance == null ? this : throw new Exception();
			InitializeComponent();
			title.Text = Path.GetFileName(rootFolderPath);
			if (title.Text.Length == 0) title.Text = Path.GetDirectoryName(rootFolderPath);

			// Tạm thời chưa làm Thanh tìm kiếm
			searchBar.MaxHeight = 0;

			#region Khởi tạo Cây thư mục
			var rootFolder = Folder.FromPath(rootFolderPath, false);
			foreach (var folder in rootFolder)
			{
				(folder_links as Dictionary<Folder, ObservableCollection<ILink>>)[folder] = new ObservableCollection<ILink>(folder.children);
				(folder_fileGenerators as Dictionary<Folder, IEnumerator<File>>)[folder] = folder.GenerateFiles();
			}
			folderTree.Items.Add(rootFolder);
			rootFolder.IsSelected = true;
			if (rootFolder.children.Count == 0)
			{
				folderTree.Items.Clear();
				column_folderTree.MaxWidth = column_splitter.MaxWidth = 0;
			}
			#endregion

			#region Nếu rootFolder có thay đổi thì restart app
			watcher = new FileSystemWatcher(rootFolderPath)
			{
				IncludeSubdirectories = true,
			};
			watcher.Changed += (s, e) => App.Restart();
			watcher.Created += (s, e) => App.Restart();
			watcher.Deleted += (s, e) => App.Restart();
			watcher.Renamed += (s, e) => App.Restart();
			watcher.Error += (s, e) => App.Restart();
			watcher.EnableRaisingEvents = true;
			#endregion
		}


		private static FileExplorer instance;
		public static async void Show(string rootFolderPath, Window owner)
		{
			if (instance == null || (instance.folderTree.Items[0] as Folder).fullPath != rootFolderPath)
			{
				instance?.Close();
				instance = null;
				instance = new FileExplorer(rootFolderPath);
			}

			instance.Owner = owner;
			instance.Show();
			await Task.Yield();
			instance.Focus();
		}


		private void OnClick_Hide(object sender, RoutedEventArgs e)
		{
#if DEBUG
			Close();
#else
			Hide();
#endif
		}


		#region Thanh tìm kiếm
		private void OnClick_VoiceSearch(object sender, RoutedEventArgs e)
		{
			throw new NotImplementedException();
		}


		private void OnSearchBoxTextChanged(object sender, TextChangedEventArgs e)
		{
			throw new NotImplementedException();
		}
		#endregion


		#region Cây thư mục
		private readonly IReadOnlyDictionary<Folder, ObservableCollection<ILink>> folder_links = new Dictionary<Folder, ObservableCollection<ILink>>();
		private readonly IReadOnlyDictionary<Folder, IEnumerator<File>> folder_fileGenerators = new Dictionary<Folder, IEnumerator<File>>();

		private void folderTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			if (e.NewValue == null) return;

			var folder = e.NewValue as Folder;
			folderContent.ItemsSource = folder_links[folder];
			if (folder_fileGenerators[folder] != null) GenerateFiles(folder);
		}


		private CancellationTokenSource cancelGeneratingFiles = new CancellationTokenSource();
		/// <summary>
		/// Tại 1 thời điểm chỉ có 1 worker-thread được truy cập <see cref="folder_fileGenerators"/>
		/// </summary>
		private readonly object lockGenerator = new object();
		private async void GenerateFiles(Folder folder)
		{
			cancelGeneratingFiles.Cancel();
			cancelGeneratingFiles.Dispose();
			var token = (cancelGeneratingFiles = new CancellationTokenSource()).Token;
			var links = folder_links[folder];
			var files = folder.files as List<File>;
			await Task.Run(GenerateFiles);


			void GenerateFiles()
			{
				lock (lockGenerator)
				{
					var generator = folder_fileGenerators[folder];
					if (generator == null) return;

					while (true)
					{
						if (token.IsCancellationRequested) return;
						if (!generator.MoveNext()) break;
						var file = generator.Current;
						App.Current.Dispatcher.InvokeAsync(() =>
						{
							files.Add(file);
							links.Add(file);
						});
					}

					App.Current.Dispatcher.Invoke(() =>
					{
						var g = folder_fileGenerators as Dictionary<Folder, IEnumerator<File>>;
						g[folder].Dispose();
						g[folder] = null;
					});
				}
			}
		}
		#endregion


		#region Tệp tin và thư mục con
		private void folderContent_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key != Key.Enter || folderContent.SelectedItem == null) return;
			OpenLink(folderContent.SelectedItem as ILink);
		}


		private void folderContent_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (folderContent.SelectedItem == null
				|| !(folderContent.ItemContainerGenerator.ContainerFromItem(folderContent.SelectedItem) as ListViewItem).IsMouseOver) return;
			OpenLink(folderContent.SelectedItem as ILink);
		}


		private void OpenLink(ILink link)
		{
			if (link is Folder)
			{
				(link as Folder).IsSelected = true;
				return;
			}

			var file = link as File;
			string EX = Path.GetExtension(file.fullPath).ToUpper();
			if (PhotoViewers.PhotoViewer.EXTENSIONS.Contains(EX))
				new PhotoViewers.PhotoViewer(file.fullPath, this).Show();
			else if (MediaPlayers.MediaPlayer.EXTENSIONS.Contains(EX))
				new MediaPlayers.MediaPlayer(file.fullPath, this).Show();
		}
		#endregion


		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (instance == this) instance = null;
			cancelGeneratingFiles.Cancel();
			cancelGeneratingFiles.Dispose();
			foreach (var kvp in folder_fileGenerators) kvp.Value?.Dispose();
			watcher.EnableRaisingEvents = false;
			watcher.Dispose();
		}
	}



	public interface ILink
	{
		string name { get; }

		string fullPath { get; }

		BitmapSource thumbnail { get; }
	}



	public sealed class File : ILink
	{
		public string name { get; private set; }

		public string fullPath { get; private set; }

		public BitmapSource thumbnail { get; private set; }

		public Folder folder { get; private set; }

		public override string ToString() => $"File: {name}, fullPath: {fullPath}";


		public File(string fullPath, Folder folder)
		{
			name = Path.GetFileNameWithoutExtension(this.fullPath = fullPath);
			thumbnail = ShellFile.FromFilePath(fullPath).Thumbnail.BitmapSource;
			thumbnail.Freeze();
			this.folder = folder;
		}
	}



	public sealed class Folder : TreeViewItem, ILink, IEnumerable<Folder>
	{
		public string name
		{
			get => Header as string;
			private set => Header = value;
		}

		public string fullPath { get; private set; }

		public BitmapSource thumbnail { get; private set; }

		public readonly IReadOnlyList<File> files = new List<File>();

		public readonly IReadOnlyList<Folder> children = new List<Folder>();

		public Folder parent { get; private set; }

		public override string ToString() => $"Folder: {name}, fullPath: {fullPath}";


		private Folder(string fullPath, Folder parent, bool generateFiles)
		{
			name = Path.GetFileName(this.fullPath = fullPath);
			thumbnail = ShellFolder.FromParsingName(fullPath).Thumbnail.BitmapSource;
			thumbnail.Freeze();
			this.parent = parent;
			if (generateFiles)
			{
				var files = this.files as List<File>;
				foreach (string f in Directory.GetFiles(fullPath)) files.Add(new File(f, this));
			}
		}


		private static List<Folder> A = new List<Folder>(), B = new List<Folder>();
		private static readonly List<string> childPaths = new List<string>();
		/// <summary>
		/// Tạo cây thư mục với gốc là thư mục <paramref name="rootPath"/><br/>
		/// </summary>
		/// <returns>Thư mục gốc </returns>
		public static Folder FromPath(string rootPath, bool generateFiles = true)
		{
			rootPath = rootPath[rootPath.Length - 1] == Path.DirectorySeparatorChar ?
				rootPath.Substring(0, rootPath.Length - 1) : rootPath;

			var root = new Folder(rootPath, null, generateFiles);
			A.Clear(); A.Add(root);
			B.Clear();
			do
			{
				foreach (var folder in A)
				{
					var children = folder.children as List<Folder>;
					var items = folder.Items;

					childPaths.Clear();
					foreach (var p in Directory.GetDirectories(folder.fullPath)) childPaths.Add(p);
					childPaths.Sort();
					foreach (var childPath in childPaths)
					{
						var child = new Folder(childPath, folder, generateFiles);
						children.Add(child);
						items.Add(child);
						B.Add(child);
					}
				}

				var tmp = A; A = B; B = tmp; B.Clear();
			} while (A.Count != 0);
			return root;
		}


		/// <summary>
		/// THREAD SAFE<para/>
		/// Nhớ thêm kết quả vào <see cref="files"/>
		/// </summary>
		public IEnumerator<File> GenerateFiles()
		{
			var paths = new List<string>();
			foreach (string path in Directory.GetFiles(fullPath)) paths.Add(path);
			paths.Sort();
			foreach (string path in paths) yield return new File(path, this);
		}


		/// <summary>
		/// Duyệt BFS với gốc là this và đi từ gốc xuống tất cả nhánh con
		/// </summary>
		/// <returns>Tất cả thư mục bao gồm this</returns>
		public IEnumerator<Folder> GetEnumerator()
		{
			A.Clear(); A.Add(this);
			B.Clear();
			do
			{
				foreach (var f in A)
				{
					yield return f;
					B.AddRange(f.children);
				}

				var tmp = A; A = B; B = tmp; B.Clear();
			} while (A.Count != 0);
		}


		/// <summary>
		/// Duyệt BFS với gốc là this và đi từ gốc xuống tất cả nhánh con
		/// </summary>
		/// <returns>Tất cả thư mục bao gồm this</returns>
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
